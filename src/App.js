import { useState } from 'react';

import {UserProvider} from './UserContext';

import {Container} from 'react-bootstrap';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import './App.css';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage'

function App() {
 

  // State hook for the user state
  // global scope
  // This will be used to store the user information and will be used for validating if a user is logged in on app or not
  const [user, setUser] = useState({email: localStorage.getItem('email')});

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    // Common patter in react.js for a component to return multiple elements.
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
  {/*Initializes the dynamic routing*/}
    <Router>
      <AppNavbar />
      <Container>
          <Routes> 
            <Route path= "/" element={<Home />} />
            <Route path= "/courses" element={<Courses />} />
            <Route path= "/register" element={<Register />} />
            <Route path= "/login" element={<Login />} />
            <Route path= "/logout" element={<Logout />} />
            {/*
              "*" - Wildcard character that will match any path that was defined in routes
            */}   
            <Route path="/*" element={<ErrorPage/>}/>
          </Routes>
      </Container>
    </Router>
    </UserProvider>
    </>
  );
}

export default App;